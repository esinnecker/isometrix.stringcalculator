using isometrix.stringcalculator.calculator;
using NUnit.Framework;
using System;

namespace NUnitCalculator
{
	[TestFixture]
	public class CalculatorTests
	{
		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public void Add_EmptyString_Return0() 
		{
			int expected = 0;
			int result = Calculator.Add("");
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void Add_OneNumber_ReturnNumber()
		{
			int expected = 1;
			int result = Calculator.Add("1");
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void Add_TwoNumbers_ReturnSum()
		{
			int expected = 3;
			int result = Calculator.Add("1,2");
			Assert.AreEqual(expected, result);
		}

		[TestCase(6, "1,2,3")]
		public void Add_MultipleNumbers_ReturnSum(int expected, string value)
		{
			int result = Calculator.Add(value);
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void Add_NewLineBetweenNumbers_ReturnSum()
		{
			int expected = 3;
			int result = Calculator.Add("1\n2");
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void Add_DifferentDelimeter_ReturnSum()
		{
			int expected = 3;
			int result = Calculator.Add("//;\n1;2");
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void Add_NegativeValue_ReturnException()
		{
			TestDelegate test = () => Calculator.Add("-1");
			Assert.Throws<Exception>(test);
		}

		[Test]
		public void Add_GreaterThan1000()
		{
			int expected = 2;
			int result = Calculator.Add("2,1000");
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void Add_CustomDelimeter()
		{
			int expected = 6;
			int result = Calculator.Add("//[***]\n1***2***3");
			Assert.AreEqual(expected, result);
		}

		/*[Test]
		public void Add_MultipleDelimeter()
		{
			int expected = 6;
			int result = Calculator.Add("//[*][%]\n1*2%3");
			Assert.AreEqual(expected, result);
		}*/
	}
}