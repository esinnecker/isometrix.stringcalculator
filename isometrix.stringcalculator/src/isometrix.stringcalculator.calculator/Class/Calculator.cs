﻿using System;
using System.Collections.Generic;

namespace isometrix.stringcalculator.calculator
{
	public static class Calculator
	{
		//List of delimiter
		public static List<string> delimeters = new List<string> { ",", "\n" };

		public static int Add(string value)
		{
			if (ValidateEmptyString(value) == "0") return 0; 

			int sum = 0;

			value = CheckDelimeter(value);

			List<int> values = ListValues(value);

			values.ForEach(s => sum += s);

			return sum;
		}

		/// <summary>
		/// Validate if string is empty. Case is empty must return 0 
		/// </summary>
		private static string ValidateEmptyString(string value)
		{
			string returnValue = value;
			if (value == "") returnValue = "0";
			return returnValue;
		}

		/// <summary>
		/// Check the delimeter is ';' or '***'
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		private static string CheckDelimeter(string value)
		{
			if (value.StartsWith("//["))
			{
				int index = value.IndexOf("\n");
				delimeters.Add(value.Substring(3, index-4));
				//delimeters.Add(value.Substring(3, index - 4).Replace("]", "").Split('[').ToString());
				value = value.Substring(index + 1);
			}
			else if (value.StartsWith("//"))
			{
				delimeters.Add(value[2].ToString());
				value = value.Substring(4);
			}
			return value;
		}

		private static List<int> ListValues(string value)
		{
			var values = new List<int>();

			Array.ForEach(
				value.Split(delimeters.ToArray(), StringSplitOptions.None),
				s =>
				{
					NegativeValueException(s);
					s = GreaterThan1000(s);
					values.Add(int.Parse(s));
				});
			return values;
		}		

		//Validate the value is negative
		private static void NegativeValueException(string s)
		{
			string message = "negatives not allowed";
			if (int.Parse(s) < 0) throw new Exception(message);
		}

		//Validate the value is greater than 1000
		private static string GreaterThan1000(string s)
		{
			string value = s;
			if (int.Parse(s) >= 1000) value = "0"; 
				return value;
		}		
	}
}
